<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReactionCommentModel extends Model
{
    protected $table = "reactions_comment";
    protected $primaryKey = "post_comment";
    protected $fillable = ["id, comment_id, reactions, created_at, updated_at, deleted_at"];
}
