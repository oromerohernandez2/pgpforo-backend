<?php

namespace App\Http\Controllers;

use App\PostModel;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = PostModel::withTrashed()->get();
        return response(json_encode($posts), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('imagepost');
        if ($request->hasFile('imagepost')) {
            $nameimage = time() . $file->getClientOriginalName();
            $file->move(public_path() . '/imagespost/', $nameimage);
        }

        $savepost = new PostModel();
        $savepost->title = $request->titlepost;
        $savepost->content = $request->contentpost;
        if ($file != '') {
            $savepost->image = $nameimage;
        } else {
            $savepost->image = 'noimagepost.jpeg';
        }
        $savepost->save();
        return response("Se creó el post", 200);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $showpost = PostModel::find($id);
        return response(json_decode($showpost), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file = $request->file('imagepost');
        if ($request->hasFile('imagepost')) {
            $nameimage = time() . $file->getClientOriginalName();
            $file->move(public_path() . '/imagespost/', $nameimage);
        }

        $updatepost = PostModel::find($id);
        $updatepost->title = $request->title;
        $updatepost->content = $request->contentpost;
        if ($file != '') {
            $updatepost->image = $nameimage;
        }
        $updatepost->save();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deletepost = PostModel::find($id);
        if($deletepost->deleted_at == null){
            $deletepost->delete();
        }else{
            $deletepost->forceDelete();
        }

    }

    public function searchpostbycrit(Request $request){
        $post = PostModel::where('title','like','%'.$request->crit.'%')->get();
        return response(json_decode($post), 200);
    }

    public function searchpostbycategory($id){
        $post = PostModel::where('category_id','=',$id)->get();
        return response(json_encode($post), 200);
    }

}
