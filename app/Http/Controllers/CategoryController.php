<?php

namespace App\Http\Controllers;

use App\CategoryModel;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = CategoryModel::withTrashed()->get();
        return response(json_decode($categories), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $saveCategory = new CategoryModel();
        $saveCategory->name = $request->namecategory;
        $saveCategory->description = $request->descriptioncategory;
        $saveCategory->save();
        return response("Se ingresó", 200);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $showcategory = CategoryModel::find($id);
        return json_encode($showcategory);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updatecategory = CategoryModel::find($id);
        $updatecategory->name = $request->namecategory;
        $updatecategory->description = $request->descriptioncategory;
        $updatecategory->save();
        return response("Se actualizo", 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = CategoryModel::withTrashed()->find($id);
        $category->forceDelete();
        return response("Se ha eliminado", 200);
    }

    public function restore($id)
    {
        CategoryModel::withTrashed()->find($id)->restore();
        return response("Se restauro la categoria", 200);

    }
}
