<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentModel extends Model
{
    protected $table = "comment";
    protected $primaryKey = "id";
    protected $fillable = ["id, post_id, user_id, status, created_at, updated_at, deleted_at"];
}
