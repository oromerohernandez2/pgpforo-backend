<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostModel extends Model
{
    protected $table = "post";
    protected $primaryKey = "id";
    protected  $fillable = ["id, title, brief, content, image, status, category_id, user_id,
                             created_at, updated_at, deleted_at"];
}
