<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryModel extends Model
{
    use  SoftDeletes;
    protected $table = "category";
    protected $primaryKey = "id";
    protected $fillable = ["id, name, description, created_at, deleted_at, updated_at"];
}
