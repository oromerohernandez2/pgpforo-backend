<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReactionPostModel extends Model
{
    protected $table = "reactions_post";
    protected $primaryKey = "id";
    protected $fillable = ["id, post_id, reactions, created_at, updated_at, deleted_at"];
}
